package using_jsoup;

public class Item {
	private String title;
	private String description;
	private String pubDate;
	private String link;
	private String imageLink;
	public String getTitle() {
		return title;
	}
	public Item(String title, String description, String pubDate, String link, String imageLink) {
		super();
		this.title = title;
		this.description = description;
		this.pubDate = pubDate;
		this.link = link;
		this.imageLink = imageLink;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPubDate() {
		return pubDate;
	}
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getImageLink() {
		return imageLink;
	}
	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}
}
