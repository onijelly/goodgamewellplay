package using_jsoup;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.traversal.DocumentTraversal;
public class DotaTwo {
	private String url;
	private ArrayList<Item> items;
	public DotaTwo(String url){
		this.url=url;
		items=new ArrayList<Item>();
	}
	public void parserItems() throws IOException{
		String title, des, date, img, link;
		Document doc = Jsoup.connect(url).get(); // connect to url and parse its conntent into a document
		Elements elements = doc.select("item"); // select your elements from the document
		for( Element element : elements ) // iterate over each elements you've selected
		{
			title=element.select("title").text();
		    des=element.select("description").text();
		    des=des.replaceAll("<p>", "");
		    des=des.replaceAll("</p>", "");
		    des=des.substring(0,des.indexOf("Click <a"));
		    if(des.indexOf("<span")>=0){
		    	des=des.substring(0,des.indexOf("<span"));
		    }
		    date=element.select("pubDate").text();
		    int i=date.indexOf(" ", date.indexOf(" ") + 1);
		    i=date.indexOf(" ", i + 1);
		    i=date.indexOf(" ", i + 1);
		    date=date.substring(0, i);
		    img=element.select("enclosure").attr("url");
		    link=element.select("link").text();
		    Item item=new Item(title, des, date, link, img);
		    items.add(item);
		}
	}
	public ArrayList<Item> getItems() throws IOException {
		parserItems();
		return items;
	}
}
