/*WebView textWView=new WebView(this);
                    textWView.setLayoutParams(lparams);
                    textWView.setBackgroundColor(Color.rgb(80,80,80));
                    String text;
                    text = "<html><body><p align=\"justify\">";
                    text+="<font color=\"white\">";
                    text+= tag.text();
                    text+="</font>";
                    text+= "</p></body></html>";
                    textWView.loadData(text, "text/html", "utf-8");
                    */
package esport.oniapp.gosugamers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class ReadNews extends AppCompatActivity {
    private TextView tvTitle;
    private LinearLayout mainLayout;
    private ProgressDialog pd;
    private int h,w;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_news);
        tvTitle = (TextView) findViewById(R.id.title_news);
        mainLayout = (LinearLayout) findViewById(R.id.news_layout);
        if(SplashActivity.isAdsEnable) {
            //do add ads here
            MobileAds.initialize(getApplicationContext(), "ca-app-pub-9647322085691192~4850597861");
            AdView mAdView = (AdView) findViewById(R.id.adView);
            mAdView.bringToFront();
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }

        Intent intent = getIntent();
        String title = intent.getExtras().getString("title");
        String link = intent.getExtras().getString("link");
        tvTitle.setText(title);
        pd=ProgressDialog.show(ReadNews.this,"Loading content","Please wait",true);
        pd.show();
        timerDelayRemoveView(8000,pd);
        try {
            Document doc = new LoadData().execute(link).get();
            Elements divText = doc.select("div.text.clearfix");
            Element divlvl2 = divText.select("div").get(0);
            divlvl2.select("section.poll.poll--quick.poll--state-vote").remove();
            divlvl2.select("div.article-author").remove();
            divlvl2.select("div.social-share-links.social-share-links-article").remove();
            divlvl2.select("a.button").remove();
            LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            WebView content = new WebView(this);
            content.setLayoutParams(lparams);
            StringBuilder text;
            text = new StringBuilder("<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/><style>img{max-height: 100%;max-width: 100%;}\n" +
                    "p {font-family:\"Tangerine\", \"Sans-serif\", \"Serif\" font-size: 48px} \n a{color: black; text-decoration: none;} \n div.tournament{max-width: 100%; display: inline-block; float:left; clear:left; border:5px solid blue;}" +
                    "div.match-hover.match{border: 1px solid red} \n" +
                    "h3.section{color: white;font-size:20px; background-color:black} \n"+
                    "div.round{border: 5px solid blue; width:100% !important;} \n"+
                    "div.round>h3{color: blue;} \n"+
                    "table, th, td { border: 1px solid black; }</style></head><body align=\"justify\">");
            text.append(divlvl2.html().replace("height=\"366\"", "height=\"auto\"").replace("width=\"650\"", "width=\"100%\""));
            text.append("</body></html>");
            //Log.i("DivTag",text.toString());
            content.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    pd.dismiss();
                    super.onPageFinished(view, url);
                }
            });
            content.setWebChromeClient(new WebChromeClient() {
            });
            content.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return (motionEvent.getAction() == MotionEvent.ACTION_MOVE);
                }
            });
            content.getSettings().setJavaScriptEnabled(true);
            content.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            content.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            content.getSettings().setLoadWithOverviewMode(true);
            content.getSettings().setUseWideViewPort(true);
            content.setVerticalScrollBarEnabled(false);
            content.getSettings().setDefaultTextEncodingName("utf-8");
            content.loadDataWithBaseURL("http://www.gosugamers.net/",text.toString(), "text/html; charset=utf-8", "utf-8","http://www.gosugamers.net/");
            mainLayout.addView(content);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private class LoadData extends AsyncTask<String, Void, Document> {

        @Override
        protected Document doInBackground(String... strings) {
            try {
                return Jsoup.parse(new URL(strings[0]).openStream(), "UTF-8", strings[0]);
            } catch (IOException e) {
                AlertDialog alertDialog = new AlertDialog.Builder(ReadNews.this).create();
                alertDialog.setTitle("Oh Sorry");
                alertDialog.setMessage("Your internet connection has a problem");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                pd.dismiss();
                            }
                        });
                alertDialog.show();
                e.printStackTrace();
            }
            return null;
        }
    }


    private class DownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {

        @Override
        protected ViewHolder doInBackground(ViewHolder... params) {
            // TODO Auto-generated method stub
            //load image directly
            ViewHolder viewHolder = params[0];
            try {
                URL imageURL = viewHolder.imageURL;
                viewHolder.bitmap = BitmapFactory.decodeStream(imageURL.openStream());
            } catch (Exception e) {
                // TODO: handle exception
                viewHolder.bitmap = null;
            }

            return viewHolder;
        }

        @Override
        protected void onPostExecute(ViewHolder result) {
            // TODO Auto-generated method stub
            if (result.bitmap == null) {
                result.imageView.setImageResource(R.drawable.orca_icon);
            } else {
                result.imageView.setImageBitmap(result.bitmap);
            }
        }
    }

    private class ViewHolder {
        Bitmap bitmap;
        URL imageURL;
        ImageView imageView;
    }

    public void timerDelayRemoveView(long time, final ProgressDialog p){
        android.os.Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                p.dismiss();
            }
        },time);
    }
}

