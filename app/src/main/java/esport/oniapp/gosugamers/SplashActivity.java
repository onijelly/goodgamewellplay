package esport.oniapp.gosugamers;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import esport.oniapp.gosugamers.MainActivity;

public class SplashActivity extends Activity {
    private final int SPLASH_TIME_OUT=1000;
    public static boolean isAdsEnable=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        LoadAds la=new LoadAds(getBaseContext());
        try {
            SplashActivity.isAdsEnable=la.execute().get().booleanValue();

        } catch (InterruptedException e) {
            e.printStackTrace();
            //ignore it
        } catch (ExecutionException e) {
            e.printStackTrace();
            //ignore it
        }

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}

class LoadAds extends AsyncTask<Void, Void, Boolean> {
    private Context context;
    public LoadAds(Context m){
        this.context=m;
    }
    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            String out = new Scanner(new URL("https://raw.githubusercontent.com/ita93/ManageAllApp/master/AppEnable").openStream(), "UTF-8").useDelimiter("\\A").next().replace("\n", "").replace("\r", "");;
            Log.i("ISENABLE","\'"+out+"\'");
            if(out.equals("1")){
                return new Boolean(true);
            }else{
                return new Boolean(false);
            }

        } catch (IOException e) {
            android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
            alertDialog.setTitle("Network problem");
            alertDialog.setMessage("Internet has some problem please check your Internet connection");
            alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
        return false;
    }
}