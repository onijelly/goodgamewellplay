package esport.oniapp.gosugamers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import adapter.MatchItemAdapter;
import adapter.PostItemAdapter;
import esport.oniapp.gosugamers.XML.DotaTwo;
import esport.oniapp.gosugamers.XML.MatchItem;
import esport.oniapp.gosugamers.XML.MatchParser;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private gameList currentGame;
    private enum gameList {DOTA, LOL, CS, OW, HS, HOTS};
    PostItemAdapter itemAdapter;
    private RecyclerView listView;
    private PostItemAdapter adapter;
    private MatchItemAdapter matchAdapter;
    private ImageView iv;
    private String gosubet_link;
    private String gosunews_link;
    private TextView tvNo;
    private FloatingActionMenu fabmn;
    private static final String GB_URL = "http://www.gosugamers.net/news/rss";
    private static final String DOTA_URL = "http://www.gosugamers.net/dota2/news/rss";
    private static final String LOL_URL = "http://www.gosugamers.net/lol/news/rss";
    private static final String CS_URL = "http://www.gosugamers.net/counterstrike/news/rss";
    private static final String HS_URL = "http://www.gosugamers.net/hearthstone/news/rss";
    private static final String OW_URL = "http://www.gosugamers.net/overwatch/news/rss";
    private static final String HOTS_URL = "http://www.gosugamers.net/heroesofthestorm/news/rss";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(SplashActivity.isAdsEnable) {
            //do add ads here
            MobileAds.initialize(getApplicationContext(), "ca-app-pub-9647322085691192~4850597861");
            AdView mAdView = (AdView) findViewById(R.id.adView);
            mAdView.bringToFront();
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tvNo= (TextView) findViewById(R.id.number_of_items);
        listView = (RecyclerView) findViewById(R.id.recycler_view);
        listView.setLayoutManager(new LinearLayoutManager(this));

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        fabmn= (FloatingActionMenu) findViewById(R.id.menu);
        fabmn.setVisibility(View.GONE);
        DotaTwo dota2 = new DotaTwo(GB_URL,listView,MainActivity.this);
        try {
            dota2.execute(GB_URL);
        } catch (Exception e) {
        }

        iv = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.imageView);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabmn.setVisibility(View.GONE);
                DotaTwo dota2 = new DotaTwo(GB_URL, listView, MainActivity.this);
                try {
                    dota2.execute(GB_URL);
                    setTitle(R.string.mainActivity_title);
                    drawer.closeDrawers();
                } catch (Exception e) {
                }
            }
        });
        actionButtonHandler();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Really Exit?")
                    .setMessage("Are you sure you want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            MainActivity.super.onBackPressed();
                            finish();
                        }
                    }).create().show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        try {
            if (id == R.id.dota2) {
                DotaTwo dota2 = new DotaTwo(DOTA_URL,listView,MainActivity.this);
                dota2.execute(DOTA_URL);
                currentGame = gameList.DOTA;
                fabmn.setVisibility(View.VISIBLE);
                setTitle(R.string.dota);
            } else if (id == R.id.lol) {
                DotaTwo dota2 = new DotaTwo(LOL_URL,listView, MainActivity.this);
                dota2.execute(LOL_URL);
                currentGame = gameList.LOL;
                fabmn.setVisibility(View.VISIBLE);
                setTitle(R.string.lol);
            } else if (id == R.id.csgo) {
                DotaTwo dota2 = new DotaTwo(CS_URL,listView, MainActivity.this);
                dota2.execute(CS_URL);
                currentGame = gameList.CS;
                fabmn.setVisibility(View.VISIBLE);
                setTitle(R.string.csgo);
            } else if (id == R.id.hs) {
                DotaTwo dota2 = new DotaTwo(HS_URL,listView, MainActivity.this);
                dota2.execute(HS_URL);
                currentGame = gameList.HS;
                fabmn.setVisibility(View.VISIBLE);
                setTitle(R.string.hs);
            } else if (id == R.id.hots) {
                DotaTwo dota2 = new DotaTwo(HOTS_URL,listView, MainActivity.this);
                dota2.execute(HOTS_URL);
                currentGame = gameList.HOTS;
                fabmn.setVisibility(View.VISIBLE);
                setTitle(R.string.hots);
            } else if (id == R.id.overwatch) {
                DotaTwo dota2 = new DotaTwo(OW_URL,listView, MainActivity.this);
                dota2.execute(OW_URL).get();
                currentGame = gameList.OW;
                fabmn.setVisibility(View.VISIBLE);
                setTitle(R.string.ow);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void actionButtonHandler() {
        com.github.clans.fab.FloatingActionButton news_btn = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.menu_item_news);
        com.github.clans.fab.FloatingActionButton up_btn = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.menu_item_up_match);
        com.github.clans.fab.FloatingActionButton live_btn = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.menu_item_live_match);
        com.github.clans.fab.FloatingActionButton end_btn = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.menu_item_end_match);

        news_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLink();
                DotaTwo dota2 = new DotaTwo(gosunews_link,listView, MainActivity.this);
                try {
                    dota2.execute(gosunews_link);
                    setTitle(R.string.mainActivity_title);
                } catch (Exception e) {

                }finally{
                    fabmn.close(true);
                }
            }
        });

        up_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    getLink();
                    MatchParser parser = new MatchParser(gosubet_link,MainActivity.this,listView,tvNo);
                    parser.execute("up");
                } catch (Exception e) {

                }finally{
                    fabmn.close(true);
                }

            }
        });

        live_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    getLink();
                    MatchParser parser = new MatchParser(gosubet_link, MainActivity.this, listView, tvNo);
                    parser.execute("live");
                } catch (Exception e) {

                } finally {
                    fabmn.close(true);
                }
            }
        });

        end_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    getLink();
                    MatchParser parser = new MatchParser(gosubet_link, MainActivity.this, listView, tvNo);
                    parser.execute("end");
                } catch (Exception e) {

                } finally {
                    fabmn.close(true);
                }
            }
        });
    }

    private void getLink(){
        tvNo.setText("");
        //set link for all
        switch (currentGame) {
            case DOTA:
                gosubet_link = MatchParser.DOTA2_MATCH;
                gosunews_link=DOTA_URL;
                setTitle(R.string.dota_match);
                break;
            case CS:
                gosubet_link = MatchParser.CS_MATCH;
                gosunews_link=CS_URL;
                setTitle(R.string.csgo_match);
                break;
            case HOTS:
                gosubet_link = MatchParser.HOTS_MATCH;
                gosunews_link=HOTS_URL;
                setTitle(R.string.hots_match);
                break;
            case HS:
                gosubet_link = MatchParser.HS_MATCH;
                gosunews_link=HS_URL;
                setTitle(R.string.hs_match);
                break;
            case OW:
                gosubet_link = MatchParser.OW_MATCH;
                gosunews_link=OW_URL;
                setTitle(R.string.ow_match);
                break;
            case LOL:
                gosubet_link=MatchParser.LOL_MATCH;
                gosunews_link=LOL_URL;
                setTitle(R.string.lol_match);
                break;
            default:
                gosunews_link=GB_URL;
                setTitle(R.string.mainActivity_title);
                break;
        }
    }

}
