package esport.oniapp.gosugamers.XML;

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

/**
 * 
 */

/**
 * @author hopev
 *
 */
public class AzubuVideo extends AsyncTask<Void,Void, String> {

	/**
	 * @param args
	 * @throws Exception 
	 * @throws JsonSyntaxException 
	 * @throws IOException 
	 */
	private String azubuUrl;
	private String m38Url;
	private String dataAccount;
	private String dataVideoID;
	private String requestUrl="https://edge.api.brightcove.com/playback/v1/accounts/countHere/videos/refHere";
	public AzubuVideo(String url){
		this.azubuUrl=url;
	}

	public void parserJson() throws JsonSyntaxException, Exception{
		Document azubu=Jsoup.connect(this.azubuUrl).get();
		Element e1=azubu.select("body").get(0);
		String e1S=e1.html().replace("&quot;", "").replace("data-setup=\"{controls:true, autoplay: true}\"", "");		
		Document azubu2=Jsoup.parse(e1S);
		String e2=azubu2.select("body").get(0).html();
		int begin=e2.indexOf("data-account")+14;
		int end=e2.indexOf("data-video-id")-2;
		dataAccount=e2.substring(begin, end);
		begin=e2.indexOf("data-video-id")+15;
		end=e2.indexOf("data-player")-2;
		dataVideoID=e2.substring(begin, end);
		requestUrl=requestUrl.replace("countHere", this.dataAccount).replace("refHere", this.dataVideoID);
		JsonElement element = new JsonParser().parse(this.readUrl(requestUrl));
		JsonObject eo=element.getAsJsonObject();
		String elemnt2String=eo.get("sources").toString().replace("[{", "").replace("}]", "").replace("://", "///").replace("\"", "");
		System.out.println(elemnt2String);
		Map<String, String> myMap = new HashMap<String, String>();
		String[] pairs = elemnt2String.split(",");
		for (int i=0;i<pairs.length;i++) {
		    String pair = pairs[i];
		    String[] keyValue = pair.split(":");
		    myMap.put(keyValue[0], keyValue[1]);		 
		}
		this.m38Url=myMap.get("src").replace("///", "://");
	}
	
	private String readUrl(String urlString) throws Exception {
		URL url = new URL(urlString);
        URLConnection urlConnection = url.openConnection();
        urlConnection.setRequestProperty("Accept", "application/json;pk=BCpkADawqM0EK2RNHjuaccclw9UloSZlamA8vmU4ZKtXm4pE6zs-vNcNWLpyEznFMEQjZlxNs9EkrXQfOPJpcTUwBOACs3aMJMG2rPnWFv_H_LlQyUtU5OC7tTxrlCjqzbrvBRkm7RIhN_8J");
        urlConnection.setConnectTimeout(1000);
        InputStream is=urlConnection.getInputStream();
        String result = getStringFromInputStream(is);
        return result;
	}
	private String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

	@Override
	protected String doInBackground(Void... voids) {
		try {
			parserJson();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.m38Url;
	}
}
