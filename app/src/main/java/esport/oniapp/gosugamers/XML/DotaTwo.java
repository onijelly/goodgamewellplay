package esport.oniapp.gosugamers.XML;

import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import adapter.PostItemAdapter;
import esport.oniapp.gosugamers.MainActivity;

public class DotaTwo extends AsyncTask<String, Void, ArrayList<Item>> {
	private String url;
	private boolean succes;
	private ArrayList<Item> items;
	private ProgressDialog pd;
	private Context context;
	private RecyclerView rec;
	private PostItemAdapter itemAdapter;
	public DotaTwo(String url, RecyclerView mrec, Context mcontext){
		this.url=url;
		items=new ArrayList<Item>();
		this.context=mcontext;
		this.rec=mrec;
	}

	public void parserItems() throws IOException{
		String title, des, date, img, link;
		Document doc = Jsoup.connect(url).get(); // connect to url and parse its conntent into a document
		Elements elements = doc.select("item"); // select your elements from the document
		for( Element element : elements ) // iterate over each elements you've selected
		{
			title=element.select("title").text();
		    des=element.select("description").text();
		    des=des.replaceAll("<p>", "");
		    des=des.replaceAll("</p>", "");
		    des=des.substring(0,des.indexOf("Click <a"));
		    if(des.indexOf("<span")>=0){
		    	des=des.substring(0,des.indexOf("<span"));
		    }
		    date=element.select("pubDate").text();
		    int i=date.indexOf(" ", date.indexOf(" ") + 1);
		    i=date.indexOf(" ", i + 1);
		    i=date.indexOf(" ", i + 1);
		    date=date.substring(0, i);
		    img=element.select("enclosure").attr("url");
		    link=element.select("link").text();
		    Item item=new Item(title, des, date, link, img);
		    items.add(item);
			itemAdapter=new PostItemAdapter(this.context,items);
		}
	}

	@Override
	protected ArrayList<Item> doInBackground(String... strings) {
		try {
			parserItems();
		} catch (IOException e) {
			succes=false;
			e.printStackTrace();
		}
		return items;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pd = ProgressDialog.show(this.context, "", "Data is loading .....", true);
		succes=true;
	}

	@Override
	protected void onPostExecute(ArrayList<Item> items) {
		super.onPostExecute(items);
		if(succes==true) {
			itemAdapter.notifyDataSetChanged();
			rec.setAdapter(itemAdapter);
			this.pd.dismiss();
		}else{
			AlertDialog alertDialog = new AlertDialog.Builder(context).create();
			alertDialog.setTitle("Network problem");
			alertDialog.setMessage("Internet has some problem please check your Internet connection");
			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							pd.dismiss();
						}
					});
			alertDialog.show();
		}
	}
}
