package esport.oniapp.gosugamers.XML;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Text;

import adapter.MatchItemAdapter;

/**
 * Created by hopev on 7/2/2016.
 */
public class MatchParser extends AsyncTask<String, Void, ArrayList<MatchItem>> {
    public static final String DOTA2_MATCH = "http://www.gosugamers.net/dota2/gosubet";
    public static final String LOL_MATCH = "http://www.gosugamers.net/lol/gosubet";
    public static final String CS_MATCH = "http://www.gosugamers.net/counterstrike/gosubet";
    public static final String OW_MATCH = "http://www.gosugamers.net/overwatch/gosubet";
    public static final String HS_MATCH = "http://www.gosugamers.net/hearthstone/gosubet";
    public static final String HOTS_MATCH = "http://www.gosugamers.net/heroesofthestorm/gosubet";

    private String url;
    private ArrayList<MatchItem> liveMatches;
    private ArrayList<MatchItem> upComingMatches;
    private ArrayList<MatchItem> resultMatches;
    private ArrayList<MatchItem> listMatch;
    private Context mContext;
    private RecyclerView rec;
    private TextView tv;
    MatchItemAdapter itemAdapter;
    ProgressDialog pd;
    private int type;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pd = ProgressDialog.show(this.mContext, "", "Data is loading .....", true);
    }

    public MatchParser(String url, Context context, RecyclerView mRec, TextView mtv) {
        this.url = url;
        liveMatches = new ArrayList<MatchItem>();
        upComingMatches = new ArrayList<MatchItem>();
        resultMatches = new ArrayList<MatchItem>();
        mContext = context;
        this.rec = mRec;
        this.tv = mtv;
    }

    public List<MatchItem> getLiveMatches() throws IOException {
        return null;
    }

    public List<MatchItem> getUpComingMatches() {
        return null;
    }

    public List<MatchItem> getResultMatches() {
        return null;
    }

    public void checkURL() {
        System.out.println(this.url);
    }

    public void parser() throws IOException {
        Document doc = Jsoup.connect(this.url).get();
        Elements epls = doc.select("table.simple.matches>tbody>tr");
        for (Element match : epls) {
            String urlMatch = "http://www.gosugamers.net" + match.select("td>a.match.hover-background").get(0).attr("href");
            String op1 = match.select("span.opp.opp1").get(0).select("span").get(0).text();
            String op2 = match.select("span.opp.opp2").get(0).select("span").get(0).text();
            String op1_flag = match.select("span.opp.opp1>span.flag").get(0).attr("title");
            op1_flag = "http://www.gosugamers.net/bilder/flags_long/" + op1_flag + ".gif";
            String op2_flag = match.select("span.opp.opp2>span.flag").get(0).attr("title");
            op2_flag = "http://www.gosugamers.net/bilder/flags_long/" + op2_flag + ".gif";
            String tour = match.select("span.tournament-icon>img").get(0).attr("src");
            int status = MatchItem.LIVE;
            String statusPrint = "LIVE";
            Element matchScore = match.select("td.type-specific>span").get(0);
            if (matchScore.hasClass("live-in")) {
                status = MatchItem.COMING;
                statusPrint = matchScore.text();
            } else {
                if (matchScore.hasClass("score-wrap")) {
                    status = MatchItem.RESULT;
                    statusPrint = matchScore.text().replace("Show", "");
                }
            }
            tour = "http://www.gosugamers.net" + tour;
            MatchItem thisMatch = new MatchItem(op1, op2, op1_flag, status, tour, op2_flag, statusPrint);
            thisMatch.setGameURL(urlMatch);
            if (status == MatchItem.LIVE) {
                liveMatches.add(thisMatch);
            } else {
                if (status == MatchItem.COMING) {
                    upComingMatches.add(thisMatch);
                } else {
                    if (status == MatchItem.RESULT) {
                        resultMatches.add(thisMatch);
                    }
                }
            }
        }
    }

    @Override
    protected ArrayList<MatchItem> doInBackground(String... strings) {
        type = -1;
        try {
            parser();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        if (strings[0].equals("live")) {
            this.listMatch = liveMatches;
            type = 0;
        } else if (strings[0].equals("up")) {
            this.listMatch = upComingMatches;
            type = 1;
        } else if (strings[0].equals("end")) {
            this.listMatch = resultMatches;
            type = 2;
        }
        itemAdapter = new MatchItemAdapter(this.mContext, this.listMatch, type);
        return this.listMatch;
    }

    @Override
    protected void onPostExecute(ArrayList<MatchItem> matchItems) {
        super.onPostExecute(matchItems);
        if(type==-1){
            AlertDialog alertDialog = new AlertDialog.Builder(this.mContext).create();
            alertDialog.setTitle("Oh Sorry");
            alertDialog.setMessage("Your internet connection has a problem");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
        if (this.listMatch==null || this.listMatch.size() < 1) {
            AlertDialog alertDialog = new AlertDialog.Builder(this.mContext).create();
            alertDialog.setTitle("Oh Sorry");
            alertDialog.setMessage("There is no match now");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            pd.dismiss();
        } else {
            itemAdapter.notifyDataSetChanged();
            this.rec.setAdapter(itemAdapter);
            this.pd.dismiss();
        }
    }
}
