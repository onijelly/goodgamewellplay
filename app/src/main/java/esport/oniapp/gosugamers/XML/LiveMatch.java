package esport.oniapp.gosugamers.XML;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import esport.oniapp.gosugamers.R;
/**
 * 
 */

/**
 * @author hopev
 *
 */
public class LiveMatch extends AsyncTask<Void,Void,ArrayList<LiveMatch.LiveMatchItem>> {

	@Override
	protected void onPostExecute(ArrayList<LiveMatchItem> liveMatchItems) {
		if (urlVideo != null && urlVideo.size() > 0) {
			ArrayAdapter<LiveMatchItem> arrayAdapter
					= new ArrayAdapter<LiveMatch.LiveMatchItem>(mContext, R.layout.my_text_view, urlVideo);
			arrayAdapter.notifyDataSetChanged();
			listView.setAdapter(arrayAdapter);
		} else
		{
			AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
			alertDialog.setTitle("Oh Sorry");
			alertDialog.setMessage("There is no available stream");
			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			alertDialog.show();
		}
	}

	@Override
	protected ArrayList<LiveMatchItem> doInBackground(Void... voids) {
		try {
			this.parser();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			isFallen=true;
			return this.urlVideo;
		}
	}

	public class LiveMatchItem  implements Serializable{
		public String videoUrl;
		public String channelName;
		public String platform;
		public LiveMatchItem(String url, String name,String platform){
			this.videoUrl=url;
			this.platform=platform;
			this.channelName=name;
		}

		@Override
		public String toString() {
			return this.platform+" - "+this.channelName;
		}
	}
	private String url;
	private Context mContext;
	private ListView listView;
	private ArrayList<LiveMatchItem> urlVideo;
	private Context context;
	private boolean isFallen;
	public LiveMatch(String url, ListView lvLiveStream, Context context) {
		this.url=url;
		this.listView=lvLiveStream;
		this.mContext=context;
		urlVideo=new ArrayList<LiveMatchItem>();
		isFallen=false;
	}
	
	public void parser() throws Exception{
		Document doc=Jsoup.parse(new URL(url).openStream(), "ISO-8859-1", url);
		Elements channel=doc.select("div.matches-streams>span.match-stream-tab");
		for(Element e:channel){
			String video_url=e.select("textarea").get(0).html().replace("&lt;", "<").replace("&gt;", ">").replace("&amp;", "&");
			String stream_channel=e.select("a.button.live.js-parent-hover").get(0).attr("title");
			String platform=e.select("label.match-stream-platform").get(0).text();
			if(platform.contains("(other)")){
				continue;
			}
			this.urlVideo.add(new LiveMatchItem(video_url,stream_channel,platform));
		}
	}
	
	public ArrayList<LiveMatchItem> getVideo(){
		return urlVideo;
	}
}
