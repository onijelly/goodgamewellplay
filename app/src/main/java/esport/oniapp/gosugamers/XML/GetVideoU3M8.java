package esport.oniapp.gosugamers.XML;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * 
 */

/**
 * @author hopev
 *
 */
public class GetVideoU3M8 extends AsyncTask<String, Void, String>{
	private String channelUrl;
	private String channelName;
	private String token;
	private String sig;
	private String tokenUrl="http://api.twitch.tv/api/channels/replacehere/access_token";
	private String m3u8Url="https://usher.ttvnw.net/api/channel/hls/replacehere.m3u8?token=replacetoken&sig=replacesig&&p=5543952";
	public GetVideoU3M8(String channelUrl) throws Exception{
		this.channelUrl=channelUrl;
	}
	
	public void createVideoUrl() throws Exception{
		this.m3u8Url=this.m3u8Url.replace("replacehere", this.channelName).replace("replacetoken", URLEncoder.encode(this.token,"UTF-8")).replace("replacesig", this.sig);
	}
	
	public void ParserJson() throws Exception{
		URL url = new URL(this.tokenUrl);
		BufferedReader br;
		br = new BufferedReader(new InputStreamReader(url.openStream()));
		StringBuilder finalString=new StringBuilder();
		String strTemp = "";
		while (null != (strTemp = br.readLine())) {
			finalString.append(strTemp);
		}
		this.tokenUrl=finalString.toString();
		JsonParser jp=new JsonParser();
		JsonObject jo=(JsonObject) jp.parse(tokenUrl);
		this.token=jo.get("token").getAsString();
		this.sig=jo.get("sig").getAsString();
	}
	
	public void getName(){
		Map<String, List<String>> listParams=getQueryParams(channelUrl);
		this.channelName=listParams.get("channel").get(0);
	}

	public String getVideoUrl(){
		return this.m3u8Url;
	}
	
	public static Map<String, List<String>> getQueryParams(String url) {
	    try {
	        Map<String, List<String>> params = new HashMap<String, List<String>>();
	        String[] urlParts = url.split("\\?");
	        if (urlParts.length > 1) {
	            String query = urlParts[1];
	            for (String param : query.split("&")) {
	                String[] pair = param.split("=");
	                String key = URLDecoder.decode(pair[0], "UTF-8");
	                String value = "";
	                if (pair.length > 1) {
	                    value = URLDecoder.decode(pair[1], "UTF-8");
	                }

	                List<String> values = params.get(key);
	                if (values == null) {
	                    values = new ArrayList<String>();
	                    params.put(key, values);
	                }
	                values.add(value);
	            }
	        }

	        return params;
	    } catch (UnsupportedEncodingException ex) {
	        throw new AssertionError(ex);
	    }
	}

	@Override
	protected String doInBackground(String... strings) {
		try{
			getName();
			this.tokenUrl=this.tokenUrl.replace("replacehere", this.channelName);
			this.m3u8Url=this.m3u8Url.replace("replacehere", this.channelName);
			ParserJson();
			createVideoUrl();
			return this.m3u8Url;
		}catch(Exception e){
			return "error";
		}
	}
}
