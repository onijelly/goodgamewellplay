package esport.oniapp.gosugamers.XML;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 
 */

/**
 * @author hopev
 *
 */
public class UpMatches {
	private String team1Name;
	private String team2Name;
	private String team1Rank;
	private String team2Rank;
	private String team1Url;
	private String team2Url;
	private String matchUrl;
	private ArrayList<TeamPerf> perfs1;
	private ArrayList<TeamPerf> perfs2;
	public UpMatches(String matchUrl) {
		super();
		this.matchUrl = matchUrl;
		this.perfs1=new ArrayList<TeamPerf>();
		this.perfs2=new ArrayList<TeamPerf>();
	}
	class TeamPerf{
		String oppName;
		String oppIcon;
		String result;
		public TeamPerf(String oppName, String oppIcon, String result) {
			super();
			this.oppName = oppName;
			this.oppIcon = oppIcon;
			this.result = result;
		}
	}
	
	public void parser() throws IOException{
		Document doc = Jsoup.connect(this.matchUrl).get();
		Element team1prefE=doc.select("div.opp1-perf").get(0);
		this.team1Name=team1prefE.select("h4>a").get(0).text();
		this.team1Url=doc.select("div.opp1-perf>h4").get(0).attr("style");
		
		this.team2Url=doc.select("div.opp2-perf>h4").get(0).attr("style");
		
		Element team2prefE=doc.select("div.opp2-perf").get(0);
		this.team2Name=team2prefE.select("h4>a").get(0).text();
		Elements recentTeam1E=team1prefE.select("div.match-list-row.hover-trigger");
		Elements recentTeam2E=team2prefE.select("div.match-list-row.hover-trigger");
		for(Element rtE: recentTeam1E){
			String oc="http://www.gosugamers.net"+rtE.select("img.icon").get(0).attr("src");
			String on=rtE.select("strong").get(0).text();
			String rs=rtE.select("span").get(0).text();
			this.perfs1.add(new TeamPerf(on, oc, rs));
		}
		for(Element rtE: recentTeam2E){
			String oc="http://www.gosugamers.net"+rtE.select("img.icon").get(0).attr("src");
			String on=rtE.select("strong").get(0).text();
			String rs=rtE.select("span").get(0).text();
			this.perfs2.add(new TeamPerf(on, oc, rs));
		}
		int index1, index2;
		index1=team1Url.indexOf("('")+2;
		index2=team1Url.lastIndexOf("')");
		team1Url="http://www.gosugamers.net"+team1Url.substring(index1, index2);
		index1=team2Url.indexOf("('")+2;
		index2=team2Url.lastIndexOf("')");
		team2Url="http://www.gosugamers.net"+team2Url.substring(index1, index2);
		team1Rank=doc.select("div.opponent.opponent1>h3>p.ranked").get(0).text();
		team2Rank=doc.select("div.opponent.opponent2>h3>p.ranked").get(0).text();
	}
	
	public static void main(String[] args) throws IOException{
		UpMatches um=new UpMatches("http://www.gosugamers.net/dota2/tournaments/10797-dota2-professional-league-season-1/matches/119094-wings-gaming-vs-lgd");
		um.parser();
	}
}
