package esport.oniapp.gosugamers.XML;

/**
 * Created by hopev on 7/2/2016.
 */
public class MatchItem {
    private String team1Name;
    private String team2Name;
    private String team1Icon;
    private String team2Icon;
    private String gameURL;
    private int status;
    private String touramentIcon;
    private String statusString;
    public static final int LIVE=0;
    public static final int COMING=1;
    public static final int RESULT=2;
    public MatchItem(String team1Name, String team2Name, String team1Icon, int status, String touramentIcon, String team2Icon,String statusString) {
        this.team1Name = team1Name;
        this.team2Name = team2Name;
        this.team1Icon = team1Icon;
        this.status = status;
        this.statusString=statusString;
        this.touramentIcon = touramentIcon;
        this.team2Icon = team2Icon;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public String getTouramentIcon() {
        return touramentIcon;
    }

    public void setTouramentIcon(String touramentIcon) {
        this.touramentIcon = touramentIcon;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTeam2Icon() {
        return team2Icon;
    }

    public void setTeam2Icon(String team2Icon) {
        this.team2Icon = team2Icon;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }

    public String getTeam1Icon() {
        return team1Icon;
    }

    public void setTeam1Icon(String team1Icon) {
        this.team1Icon = team1Icon;
    }
    public String getStatusString() {
        return statusString;
    }

    public void setStatusString(String statusString) {
        this.statusString = statusString;
    }

    public String getGameURL() {
        return gameURL;
    }

    public void setGameURL(String gameURL) {
        this.gameURL = gameURL;
    }

    @Override
	public String toString() {
		return "MatchItem [team1Name=" + team1Name + ", team2Name=" + team2Name + ", team1Icon=" + team1Icon
				+ ", team2Icon=" + team2Icon + ", status=" + status + ", touramentIcon=" + touramentIcon + "]";
	}
    
}
