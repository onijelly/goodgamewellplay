package esport.oniapp.gosugamers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.renderscript.RenderScript;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.brightcove.player.model.Video;
import com.brightcove.player.view.BrightcoveExoPlayerVideoView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.concurrent.ExecutionException;

import esport.oniapp.gosugamers.XML.AzubuVideo;
import esport.oniapp.gosugamers.XML.GetAzubuVideo;
import esport.oniapp.gosugamers.XML.GetVideoU3M8;
import esport.oniapp.gosugamers.XML.LiveMatch;

public class LiveMatchActivity extends AppCompatActivity {
    private ProgressDialog pd;
    private VideoView videoView;
    private BrightcoveExoPlayerVideoView brightcoveVideoView;
    private WebView streamView;
    private String embeddedUrl;
    private Video video;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_match);
        Intent intent = getIntent();
        videoView= (VideoView) findViewById(R.id.video_view);
        streamView = (WebView)findViewById(R.id.web_view);
        brightcoveVideoView = (BrightcoveExoPlayerVideoView) findViewById(R.id.brightcove_video_view);
        if(SplashActivity.isAdsEnable) {
            //do add ads here
            MobileAds.initialize(getApplicationContext(), "ca-app-pub-9647322085691192~4850597861");
            AdView mAdView = (AdView) findViewById(R.id.adView);
            mAdView.bringToFront();
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }
        String frame = intent.getExtras().getString("htmlcode");
        Document doc= Jsoup.parse(frame);
        embeddedUrl=doc.select("iframe").get(0).attr("src");
        if(embeddedUrl.contains("twitch")==true){
            playTwitch(embeddedUrl);
        }else if(embeddedUrl.contains("azubu")==true){
            try {
                playAzubu(embeddedUrl);
            }catch(Exception e){
                e.printStackTrace();
            }
        }else{
            playByWebView(frame);
        }
    }

    public void playTwitch(String embeddedUrl){
        videoView.setVisibility(View.VISIBLE);
        streamView.setVisibility(View.GONE);
        brightcoveVideoView.setVisibility(View.GONE);
        pd = ProgressDialog.show(LiveMatchActivity.this, "", "Buffering video...", true);
        pd.setCancelable(false);
        try {
            GetVideoU3M8 gv3 = new GetVideoU3M8(embeddedUrl);
            videoView.setVideoURI(Uri.parse(gv3.execute().get()));
            videoView.setBackgroundColor(Color.TRANSPARENT);
            MediaController mc = new MediaController(LiveMatchActivity.this);

            mc.setVisibility(View.VISIBLE);
            videoView.setMediaController(mc);

            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                    pd.dismiss();
                    videoView.setVisibility(View.VISIBLE);
                }
            });
            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    mediaPlayer.stop();
                    pd.dismiss();
                    AlertDialog alertDialog = new AlertDialog.Builder(LiveMatchActivity.this).create();
                    alertDialog.setTitle("Oh Sorry");
                    alertDialog.setMessage("This stream is offline now");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    finish();
                    return true;
                }
            });
        }catch(Exception e){
            e.printStackTrace();
            pd.dismiss();
        }
    }

    public void playAzubu(String embeddedUrl) throws ExecutionException, InterruptedException {
        videoView.setVisibility(View.GONE);
        streamView.setVisibility(View.GONE);
        brightcoveVideoView.setVisibility(View.VISIBLE);
        AzubuVideo gav=new AzubuVideo(embeddedUrl);
        pd = ProgressDialog.show(LiveMatchActivity.this, "", "Buffering video...", true);
        video=Video.createVideo(gav.execute().get());
        brightcoveVideoView.add(video);
        brightcoveVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                brightcoveVideoView.start();
                pd.dismiss();
                videoView.setVisibility(View.VISIBLE);
            }
        });
    }

    public void playByWebView(String frame){
        videoView.setVisibility(View.GONE);
        brightcoveVideoView.setVisibility(View.GONE);
        streamView.setVisibility(View.VISIBLE);
        pd=ProgressDialog.show(LiveMatchActivity.this,"Loading video","Please wait",false);
        streamView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                pd.dismiss();
                super.onPageFinished(view, url);
            }
        });
        streamView.setWebChromeClient(new WebChromeClient());
        streamView.getSettings().setJavaScriptEnabled(true);
        streamView.getSettings().setPluginState(WebSettings.PluginState.ON);
        streamView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        if (Build.VERSION.SDK_INT >= 19) {
            // chromium, enable hardware acceleration
            streamView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            streamView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        streamView.getSettings().setLoadsImagesAutomatically(true);
        streamView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        //content.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        streamView.getSettings().setLoadWithOverviewMode(true);
        streamView.getSettings().setUseWideViewPort(true);
        frame=frame.replace("width=\"799\"", "width=\"100%\"").replace("height=\"446\"", "height=\"100%\" align=\"middle\"");
        //streamView.loadData(frame, "text/html", "utf-8");
        streamView.loadUrl(embeddedUrl);
    }
}
