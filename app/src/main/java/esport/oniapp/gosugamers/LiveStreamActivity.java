package esport.oniapp.gosugamers;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import esport.oniapp.gosugamers.XML.LiveMatch;

public class LiveStreamActivity extends AppCompatActivity {
    private String urlMatch;
    private ArrayList<LiveMatch.LiveMatchItem> streams;
    private ListView lvLiveStream;
    private LiveMatch thisMatch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_stream);
        this.urlMatch=getIntent().getStringExtra("urlMatch").toString();
        lvLiveStream = (ListView)findViewById(R.id.lv_live_stream);
        thisMatch=new LiveMatch(this.urlMatch, lvLiveStream, LiveStreamActivity.this);
        try {
            thisMatch.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        lvLiveStream.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String transfer = getFrame(lvLiveStream.getAdapter().getItem(position).toString());
                Intent intent=new Intent(LiveStreamActivity.this, LiveMatchActivity.class);
                intent.putExtra("htmlcode",transfer);
                startActivity(intent);
            }
        });
    }

    private String getFrame(String name){
        for(LiveMatch.LiveMatchItem findingMatch:thisMatch.getVideo()){
            String tmp=findingMatch.platform+" - "+findingMatch.channelName;
            if(tmp.equals(name)){
                return findingMatch.videoUrl;
            }
        }
        return null;
    }
}
