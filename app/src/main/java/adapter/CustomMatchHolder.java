package adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import esport.oniapp.gosugamers.LiveMatchActivity;
import esport.oniapp.gosugamers.LiveStreamActivity;
import esport.oniapp.gosugamers.MainActivity;
import esport.oniapp.gosugamers.R;
import esport.oniapp.gosugamers.XML.LiveMatch;

/**
 * Created by hopev on 7/4/2016.
 */
public class CustomMatchHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {
    protected TextView team1TV;
    protected TextView team2TV;
    protected ImageView team1Icon;
    protected ImageView team2Icon;
    protected ImageView tourIcon;
    protected TextView gameStatus;
    protected String urlMatch;
    public ArrayList<LiveMatch.LiveMatchItem> streams;
    private ItemClickListener clickListener;
    private Context mcontext;
    public CustomMatchHolder(View itemView) {
        super(itemView);
        mcontext=itemView.getContext();
        this.team1TV= (TextView) itemView.findViewById(R.id.team1_name);
        this.team2TV= (TextView) itemView.findViewById(R.id.team2_name);
        this.team1Icon= (ImageView) itemView.findViewById(R.id.team1_icon);
        this.team2Icon= (ImageView) itemView.findViewById(R.id.team2_icon);
        this.tourIcon= (ImageView) itemView.findViewById(R.id.tourament_icon);
        this.gameStatus=(TextView)itemView.findViewById(R.id.status);
        itemView.setOnClickListener(this);
        itemView.setOnCreateContextMenuListener(this);
    }

    public void setClickListener(ItemClickListener itemClickListener){
        this.clickListener=itemClickListener;
    }

    @Override
    public void onClick(View view) {
        clickListener.onClick(view, getPosition(), false);
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.setHeaderTitle("Available Streams");
        Log.i("Stream", this.urlMatch);
        if(!this.gameStatus.getText().toString().equals("LIVE")){
            return;
        }
        try {
            String transfer = this.urlMatch;
            Intent intent=new Intent(mcontext, LiveStreamActivity.class);
            intent.putExtra("urlMatch",transfer);
            mcontext.startActivity(intent);
            /*streams=thisMatch.execute().get();
            if(streams !=null && streams.size()>0){
                for(LiveMatch.LiveMatchItem lvm:streams){
                    MenuItem mi=contextMenu.add(0,view.getId(),0,lvm.channelName);
                    mi.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            String transfer = getFrame(menuItem.getTitle().toString());
                            Intent intent=new Intent(itemView.getContext(), LiveMatchActivity.class);
                            intent.putExtra("htmlcode",transfer);
                            itemView.getContext().startActivity(intent);
                            return false;
                        }
                    });
                }
            }else {
                AlertDialog alertDialog = new AlertDialog.Builder(mcontext).create();
                alertDialog.setTitle("Oh Sorry");
                alertDialog.setMessage("There is no available stream");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getFrame(String name){
        for(LiveMatch.LiveMatchItem findingMatch:streams){
            if(findingMatch.channelName.equals(name)){
                return findingMatch.videoUrl;
            }
        }
        return null;
    }
}
