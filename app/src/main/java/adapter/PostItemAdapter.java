/**
 * PostItemAdapter.java
 * <p/>
 * Adapter Class which configs and returns the View for ListView
 */
package adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import esport.oniapp.gosugamers.MainActivity;
import esport.oniapp.gosugamers.R;
import esport.oniapp.gosugamers.ReadNews;
import esport.oniapp.gosugamers.XML.Item;

public class PostItemAdapter extends RecyclerView.Adapter<CustomViewHolder> {
    private Context mContext;
    private List<Item> datas;
    public ProgressDialog pd;
    public PostItemAdapter(){

    }
    public PostItemAdapter(Context context, List<Item> data) {
        this.mContext = context;
        this.datas = data;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.postitem, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        final Item feedItem = datas.get(position);
        if (feedItem.getImageLink() != null && !feedItem.getImageLink().isEmpty()) {
            Picasso.with(mContext).load(feedItem.getImageLink())
                    .error(R.drawable.orca_icon)
                    .placeholder(R.drawable.orca_icon)
                    .into(holder.img);
        }
        holder.titleTV.setText(feedItem.getTitle());
        holder.dateTV.setText(feedItem.getPubDate());
        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if(isLongClick){
                    //do nothing
                }else{
                    Intent intent=new Intent(mContext, ReadNews.class);
                    Item currentItem=datas.get(position);
                    intent.putExtra("title",currentItem.getTitle());
                    intent.putExtra("link",currentItem.getLink());
                    mContext.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != datas ? datas.size() : 0);
    }
}