package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import esport.oniapp.gosugamers.R;
import esport.oniapp.gosugamers.XML.Item;
import esport.oniapp.gosugamers.XML.MatchItem;

/**
 * Created by hopev on 7/4/2016.
 */
public class MatchItemAdapter extends RecyclerView.Adapter<CustomMatchHolder> {
    Context myContext;
    List<MatchItem> matches;
    int type; //0: live, 1: up; 2: end
    public MatchItemAdapter(Context context, List<MatchItem> data, int type){
        this.myContext=context;
        this.matches=data;
        this.type=type;
    }

    @Override
    public CustomMatchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.match_item,parent,false);
        CustomMatchHolder holder=new CustomMatchHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CustomMatchHolder holder, int position) {
        final MatchItem feedItem = matches.get(position);
        holder.team1TV.setText(feedItem.getTeam1Name());
        holder.team2TV.setText(feedItem.getTeam2Name());
        holder.urlMatch=feedItem.getGameURL();
        holder.gameStatus.setText(feedItem.getStatusString());
        if (feedItem.getTouramentIcon() != null && !feedItem.getTouramentIcon().isEmpty()) {
            Picasso.with(myContext).load(feedItem.getTouramentIcon())
                    .error(R.drawable.orca_icon)
                    .placeholder(R.drawable.orca_icon)
                    .into(holder.tourIcon);
        }

        if (feedItem.getTeam1Icon() != null && !feedItem.getTeam1Icon().isEmpty()) {
            Picasso.with(myContext).load(feedItem.getTeam1Icon())
                    .error(R.drawable.orca_icon)
                    .placeholder(R.drawable.orca_icon)
                    .into(holder.team1Icon);
        }

        if (feedItem.getTeam2Icon() != null && !feedItem.getTeam2Icon().isEmpty()) {
            Picasso.with(myContext).load(feedItem.getTeam2Icon())
                    .error(R.drawable.orca_icon)
                    .placeholder(R.drawable.orca_icon)
                    .into(holder.team2Icon);
        }

        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                view.showContextMenu();
                if(isLongClick){
                    return;
                }
                MatchItem item=matches.get(position);
                switch(type){
                    case 0:

                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return matches.size();
    }
}
