package adapter;

import android.view.View;

/**
 * Created by hopev on 7/1/2016.
 */
public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
