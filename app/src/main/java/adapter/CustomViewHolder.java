package adapter;

import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import esport.oniapp.gosugamers.R;
import esport.oniapp.gosugamers.XML.Item;

/**
 * Created by hopev on 6/30/2016.
 */
public class CustomViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
    private ItemClickListener clickListener;
    protected ImageView img;
    protected TextView titleTV;
    protected TextView dateTV;
    public CustomViewHolder(View itemView) {
        super(itemView);
        this.img=(ImageView) itemView.findViewById(R.id.postThumb);
        this.titleTV= (TextView) itemView.findViewById(R.id.postTitleLabel);
        this.dateTV= (TextView) itemView.findViewById(R.id.postDateLabel);
        itemView.setOnClickListener(this);
    }

    public void setClickListener(ItemClickListener itemClickListener){
        this.clickListener=itemClickListener;
    }

    @Override
    public void onClick(View view) {
        clickListener.onClick(view, getPosition(),false);
    }
}
